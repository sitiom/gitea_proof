# Codeberg Proof

This is an OpenPGP proof that connects my OpenPGP key to this Gitea account. For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:9763AE10706A1E8FCF74E5AEF6FD3820BCBC1A98]
